FIGHT 215
---------

This module adds a countdown banner linking to https://www.fight215.org/ to the
top of each page.

To disable the banner, add the following code to the site's settings.php file:

  $conf['fight215_count'] = FALSE;

This module also provides blocks with Fight 215 graphics which you can enable.
